
package indi.yh.sample.cxf.test.client.ws.service.hello;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

/**
 * This class was generated by Apache CXF 2.6.1
 * 2017-09-11T14:27:31.476+08:00
 * Generated source version: 2.6.1
 * 
 */
public final class HelloWs_HelloWsImplPort_Client {

    private static final QName SERVICE_NAME = new QName("http://service.ws.cxf.sample.yh.indi/", "HelloWsImplService");

    private HelloWs_HelloWsImplPort_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = HelloWsImplService.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        HelloWsImplService ss = new HelloWsImplService(wsdlURL, SERVICE_NAME);
        HelloWs port = ss.getHelloWsImplPort();  
        
        {
        System.out.println("Invoking sayHello...");
        java.lang.String _sayHello_msgRq = "";
        java.lang.String _sayHello__return = port.sayHello(_sayHello_msgRq);
        System.out.println("sayHello.result=" + _sayHello__return);


        }
        {
        System.out.println("Invoking noResponse...");
        java.lang.String _noResponse_msgRq = "";
        port.noResponse(_noResponse_msgRq);


        }
        {
        System.out.println("Invoking noRequest...");
        java.lang.String _noRequest__return = port.noRequest();
        System.out.println("noRequest.result=" + _noRequest__return);


        }

        System.exit(0);
    }

}
