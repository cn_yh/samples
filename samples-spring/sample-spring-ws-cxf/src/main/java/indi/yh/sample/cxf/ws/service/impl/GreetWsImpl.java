package indi.yh.sample.cxf.ws.service.impl;

import javax.jws.WebParam;
import javax.jws.WebService;

import indi.yh.sample.cxf.ws.service.GreetWs;

/**
 * <p>标题: Greet.java</p>
 * <p>业务描述:统一运维及安全管理平台</p>
 * <p>公司:东华软件股份公司</p>
 * <p>版权:dhcc2013</p>
 * @author 于鸿
 * @date 2014年5月12日
 * @version V1.0 
 */
@WebService(targetNamespace="http://service.ws.cxf.sample.yh.indi/",endpointInterface = "indi.yh.sample.cxf.ws.service.GreetWs")
public class GreetWsImpl implements GreetWs{
	
	@Override
    public String GreetA(@WebParam(name="msgRq")String msg) {
    	System.out.println("GreetA: "+msg+" coming! return 'GreetA: Hello "+msg+"'");
        return "GreetA: Hello " + msg;
    }
    
	@Override
    public String GreetB(@WebParam(name="msgRq")String msg) {
    	System.out.println("GreetB: "+msg+" coming! return 'GreetB: Hello "+msg+"'");
        return "GreetB: Hello " + msg;
    }
	
	@Override
    public String GreetC(@WebParam(name="msgRq")String msg) {
    	System.out.println("GreetC: "+msg+" coming! return 'GreetC: Hello "+msg+"'");
        return "GreetC: Hello " + msg;
    }

}
