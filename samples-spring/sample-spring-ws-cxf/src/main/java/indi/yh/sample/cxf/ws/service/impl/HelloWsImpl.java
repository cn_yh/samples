package indi.yh.sample.cxf.ws.service.impl;

import java.util.Random;

import javax.jws.WebParam;
import javax.jws.WebService;

import indi.yh.sample.cxf.ws.service.HelloWs;
/**
 * <p>标题: Hello.java</p>
 * <p>业务描述:统一运维及安全管理平台</p>
 * <p>公司:东华软件股份公司</p>
 * <p>版权:dhcc2013</p>
 * @author 于鸿
 * @date 2014年5月10日
 * @version V1.0 
 */
@WebService(targetNamespace="http://service.ws.cxf.sample.yh.indi/",endpointInterface = "indi.yh.sample.cxf.ws.service.HelloWs")
public class HelloWsImpl implements HelloWs{

	@Override
    public String sayHello(@WebParam(name="msgRq")String name) {
    	System.out.println("sayHello: "+name+" coming! return 'Hello "+name+"'");
        return "Hello " + name;
    }
    
	@Override
    public void noResponse(@WebParam(name="msgRq")String name) {
    	System.out.println("noResponse: "+name+" coming! no return");
    }
    
	@Override
    public String noRequest() {
    	Random random = new Random();
    	String result=String.valueOf(Math.abs(random.nextInt())%100);
    	System.out.println("noRequest: return '"+result+"'");
    	return result;
    }

}

