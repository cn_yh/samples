package indi.yh.sample.cxf;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import indi.yh.sample.cxf.ws.service.GreetWs;
import indi.yh.sample.cxf.ws.service.HelloWs;
import indi.yh.sample.cxf.ws.service.impl.GreetWsImpl;
import indi.yh.sample.cxf.ws.service.impl.HelloWsImpl;

@SpringBootApplication
public class Application {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public ServletRegistrationBean cxfServletRegistration() {
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new CXFServlet());
		registrationBean.setName("cxfServlet");
		registrationBean.setOrder(600);
		registrationBean.addUrlMappings("/ws/*");

		return registrationBean;
	}
	
	@Bean(name = Bus.DEFAULT_BUS_ID)
	public SpringBus springBus() {
		return new SpringBus();
	}

	@Bean
	public GreetWs greetWs() {
		return new GreetWsImpl();
	}

	@Bean
	public HelloWs helloWs() {
		return new HelloWsImpl();
	}
	
	@Bean
	public Endpoint greetEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(springBus(), greetWs());
		endpoint.publish("/greet");
		return endpoint;
	}

	@Bean
	public Endpoint helloEndpoint() {
		EndpointImpl endpoint = new EndpointImpl(springBus(), helloWs());
		endpoint.publish("/hello");
		return endpoint;
	}
}
