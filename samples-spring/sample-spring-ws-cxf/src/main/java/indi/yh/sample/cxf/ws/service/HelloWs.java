package indi.yh.sample.cxf.ws.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
/**
 * <p>标题: Hello.java</p>
 * <p>业务描述:统一运维及安全管理平台</p>
 * <p>公司:东华软件股份公司</p>
 * <p>版权:dhcc2013</p>
 * @author 于鸿
 * @date 2014年5月10日
 * @version V1.0 
 */
@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public interface HelloWs {

	@WebResult(name="msgRp")
    public String sayHello(@WebParam(name="msgRq")String name);
    
	@WebResult(name="msgRp")
    public void noResponse(@WebParam(name="msgRq")String name);
	
	@WebResult(name="msgRp")
    public String noRequest();

}

