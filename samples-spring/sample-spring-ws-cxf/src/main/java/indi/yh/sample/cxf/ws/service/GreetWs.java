package indi.yh.sample.cxf.ws.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * <p>标题: Greet.java</p>
 * <p>业务描述:统一运维及安全管理平台</p>
 * <p>公司:东华软件股份公司</p>
 * <p>版权:dhcc2013</p>
 * @author 于鸿
 * @date 2014年5月12日
 * @version V1.0 
 */
@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public interface GreetWs {
	
	@WebResult(name="msgRp")
	@WebMethod(action="GreetA")
    public String GreetA(@WebParam(name="msgRq")String msg);
    
	@WebResult(name="msgRp")
	@WebMethod(action="GreetB")
    public String GreetB(@WebParam(name="msgRq")String msg);
	
	@WebResult(name="msgRp")
	@WebMethod(action="GreetC")
    public String GreetC(@WebParam(name="msgRq")String msg);
  
}
