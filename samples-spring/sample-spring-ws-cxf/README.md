
# sample-spring-ws-cxf #

本示基于spring boot，演示了使用cxf发布webservice的示例。

示例内容包括：
* 使用cxf发布webservice服务。  
    * 简单示例：GreetWs、HelloWs  
* 使用cxf根据wsdl生成webservice客户端。  
    * src/test/java目录下的indi.yh.sample.cxf.test.client.ws包下的类为使用cxf根据wsdl生成的webservice客户端代码。


## 其他说明 ##

* 使用cxf工具生成WebService客户端代码的命令：  

> 进入cxf的bin目录，执行以下命令生成WebService客户端代码：

    直接根据wsdl地址生成:
    wsdl2java -p indi.yh.sample.cxf.test.client.ws.service.greet -encoding utf-8 -client -impl http://127.0.0.1:8080/ws/greet?wsdl
    
    wsdl2java -p indi.yh.sample.cxf.test.client.ws.service.hello -encoding utf-8 -client -impl http://127.0.0.1:8080/ws/hello?wsdl

    