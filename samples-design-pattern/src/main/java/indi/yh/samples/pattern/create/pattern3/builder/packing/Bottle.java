package indi.yh.samples.pattern.create.pattern3.builder.packing;

public class Bottle implements Packing {

	@Override
	public String pack() {
		return "Bottle";
	}

}