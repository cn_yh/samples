package indi.yh.samples.pattern.create.pattern1.factory.type.abstractFactory;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.benz.BenzCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.bmw.BmwCar;

/**
 * 
 * 描述: 抽象工厂模式<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月25日 下午1:56:42
 * @version V1.0
 */
public abstract class DriverAbstractFactory {
	
    public abstract BenzCar createBenzCar(String car) throws Exception;  
    
    public abstract BmwCar createBmwCar(String car) throws Exception;  
      
    public abstract AudiCar createAudiCar(String car) throws Exception;  

}
