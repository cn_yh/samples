package indi.yh.samples.pattern.create.pattern3.builder.packing;

public interface Packing {

	String pack();

}
