package indi.yh.samples.pattern.create.pattern1.factory.car.bmw;

public abstract class BmwCar {

	public abstract void drive();

}
