package indi.yh.samples.pattern.create.pattern1.factory.car.bmw;

public class BmwSportCar extends BmwCar {

	@Override
	public void drive() {
        System.out.println("----BmwSportCar-----------------------");  		
	}

}
