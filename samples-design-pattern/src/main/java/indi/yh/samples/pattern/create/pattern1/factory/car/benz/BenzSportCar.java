package indi.yh.samples.pattern.create.pattern1.factory.car.benz;

public class BenzSportCar extends BenzCar {

	@Override
	public void drive() {
		System.out.println("----BenzSportCar-----------------------");
	}

}
