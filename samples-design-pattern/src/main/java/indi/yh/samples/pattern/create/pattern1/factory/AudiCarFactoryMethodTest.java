package indi.yh.samples.pattern.create.pattern1.factory;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.type.factoryMethod.AudiBusinessCarFactory;
import indi.yh.samples.pattern.create.pattern1.factory.type.factoryMethod.AudiCarFactoryMethod;
import indi.yh.samples.pattern.create.pattern1.factory.type.factoryMethod.AudiSportCarFactory;

public class AudiCarFactoryMethodTest {

	public static void main(String[] args) {
		AudiCarFactoryMethod audiCarSimpleFactory = new AudiSportCarFactory();
		AudiCar audiCar = audiCarSimpleFactory.getAudiCar();
		audiCar.drive();

		audiCarSimpleFactory = new AudiBusinessCarFactory();
		audiCar = audiCarSimpleFactory.getAudiCar();
		audiCar.drive();
	}
}
