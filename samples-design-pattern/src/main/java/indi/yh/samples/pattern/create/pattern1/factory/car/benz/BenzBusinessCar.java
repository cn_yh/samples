package indi.yh.samples.pattern.create.pattern1.factory.car.benz;

public class BenzBusinessCar extends BenzCar {

	@Override
	public void drive() {
		System.out.println("----BenzBusinessCar-----------------------");
	}

}
