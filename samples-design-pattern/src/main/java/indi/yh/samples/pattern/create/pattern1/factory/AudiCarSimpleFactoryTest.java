package indi.yh.samples.pattern.create.pattern1.factory;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.type.sampleFactory.AudiCarSimpleFactory;

public class AudiCarSimpleFactoryTest {

	public static void main(String[] args) {
		AudiCarSimpleFactory audiCarSimpleFactory = new AudiCarSimpleFactory();
		AudiCar audiCar = audiCarSimpleFactory.getAudiCar("SPORT");
		audiCar.drive();

		audiCar = audiCarSimpleFactory.getAudiCar("BUSINESS");
		audiCar.drive();
	}
}
