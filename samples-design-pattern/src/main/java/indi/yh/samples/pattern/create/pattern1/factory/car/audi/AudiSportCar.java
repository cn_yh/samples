package indi.yh.samples.pattern.create.pattern1.factory.car.audi;

public class AudiSportCar extends AudiCar {

	@Override
	public void drive() {
        System.out.println("----AudiSportCar-----------------------");  		
	}

}
