package indi.yh.samples.pattern.create.pattern2.singleton.enumType;

/**
 * 
 * 描述: 枚举<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月25日 下午2:15:59
 * @version V1.0
 */
public enum EnumSingleton {

	INSTANCE;
	public void whateverMethod() {
	}
}
