package indi.yh.samples.pattern.create.pattern3.builder.coldDrink;

import indi.yh.samples.pattern.create.pattern3.builder.Item;
import indi.yh.samples.pattern.create.pattern3.builder.packing.Bottle;
import indi.yh.samples.pattern.create.pattern3.builder.packing.Packing;

public abstract class AbstractColdDrink implements Item{


	@Override
	public Packing packing() {
		return new Bottle();
	}

	@Override
	public abstract float price();

}
