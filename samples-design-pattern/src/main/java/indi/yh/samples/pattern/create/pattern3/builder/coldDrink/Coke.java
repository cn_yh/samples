package indi.yh.samples.pattern.create.pattern3.builder.coldDrink;

public class Coke extends AbstractColdDrink{

	@Override
	public String name() {
		return "Coke";
	}

	@Override
	public float price() {
		return 30.0f;
	}

}
