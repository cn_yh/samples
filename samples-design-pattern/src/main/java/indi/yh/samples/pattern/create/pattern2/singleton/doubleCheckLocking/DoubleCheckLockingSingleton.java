package indi.yh.samples.pattern.create.pattern2.singleton.doubleCheckLocking;

/**
 * 
 * 描述: 双检锁/双重校验锁（DCL，即 double-checked locking）<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月25日 下午2:05:51
 * @version V1.0
 */
public class DoubleCheckLockingSingleton {
	
	private volatile static DoubleCheckLockingSingleton singleton;

	private DoubleCheckLockingSingleton() {
	}

	public static DoubleCheckLockingSingleton getSingleton() {
		if (singleton == null) {
			synchronized (DoubleCheckLockingSingleton.class) {
				if (singleton == null) {
					singleton = new DoubleCheckLockingSingleton();
				}
			}
		}
		return singleton;
	}

}
