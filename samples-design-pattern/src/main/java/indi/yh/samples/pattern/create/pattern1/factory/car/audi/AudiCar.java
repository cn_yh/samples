package indi.yh.samples.pattern.create.pattern1.factory.car.audi;

public abstract class AudiCar {

	public abstract void drive();

}
