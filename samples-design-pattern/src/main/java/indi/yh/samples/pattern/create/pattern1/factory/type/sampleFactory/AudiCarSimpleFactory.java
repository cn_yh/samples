package indi.yh.samples.pattern.create.pattern1.factory.type.sampleFactory;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiBusinessCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiSportCar;

/**
 * 
 * 描述: 简单工厂模式<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月24日 下午4:01:19
 * @version V1.0
 */
public class AudiCarSimpleFactory {

	public AudiCar getAudiCar(String carType) {
		if (carType == null) {
			return null;
		}
		if (carType.equalsIgnoreCase("SPORT")) {
			return new AudiSportCar();
		} else if (carType.equalsIgnoreCase("BUSINESS")) {
			return new AudiBusinessCar();
		}
		return null;
	}
}
