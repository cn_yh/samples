package indi.yh.samples.pattern.create.pattern3.builder;

import indi.yh.samples.pattern.create.pattern3.builder.burger.ChickenBurger;
import indi.yh.samples.pattern.create.pattern3.builder.burger.VegBurger;
import indi.yh.samples.pattern.create.pattern3.builder.coldDrink.Coke;
import indi.yh.samples.pattern.create.pattern3.builder.coldDrink.Pepsi;

public class MealBuilder {
	
	public Meal prepareVegMeal() {
		Meal meal = new Meal();
		meal.addItem(new VegBurger());
		meal.addItem(new Coke());
		return meal;
	}

	public Meal prepareNonVegMeal() {
		Meal meal = new Meal();
		meal.addItem(new ChickenBurger());
		meal.addItem(new Pepsi());
		return meal;
	}

}
