package indi.yh.samples.pattern.create.pattern1.factory.type.factoryMethod;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;

/**
 * 
 * 描述: 工厂方法模式<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月24日 下午4:01:52
 * @version V1.0
 */
public interface AudiCarFactoryMethod {
	
	public AudiCar getAudiCar();
	
}
