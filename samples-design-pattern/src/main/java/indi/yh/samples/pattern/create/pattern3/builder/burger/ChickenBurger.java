package indi.yh.samples.pattern.create.pattern3.builder.burger;

public class ChickenBurger extends AbstractBurger{

	@Override
	public String name() {
		return "Chicken Burger";
	}

	@Override
	public float price() {
		return 50.5f;
	}

}