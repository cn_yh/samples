package indi.yh.samples.pattern.create.pattern3.builder.packing;

public class Wrapper implements Packing {

	@Override
	public String pack() {
		return "Wrapper";
	}

}
