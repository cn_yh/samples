package indi.yh.samples.pattern.create.pattern2.singleton.hungry;

/**
 * 
 * 描述: 饿汉式<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月25日 下午2:04:04
 * @version V1.0
 */
public class HungrySingleton {
	
	private static HungrySingleton instance = new HungrySingleton();

	private HungrySingleton() {
	}

	public static HungrySingleton getInstance() {
		return instance;
	}

}
