package indi.yh.samples.pattern.create.pattern1.factory.type.abstractFactory;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiSportCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.benz.BenzCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.benz.BenzSportCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.bmw.BmwCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.bmw.BmwSportCar;

public class SportDriverFactory extends DriverAbstractFactory {

	@Override
	public BenzCar createBenzCar(String car) throws Exception {
		return new BenzSportCar();
	}

	@Override
	public BmwCar createBmwCar(String car) throws Exception {
		return new BmwSportCar();
	}

	@Override
	public AudiCar createAudiCar(String car) throws Exception {
		return new AudiSportCar();
	}

}
