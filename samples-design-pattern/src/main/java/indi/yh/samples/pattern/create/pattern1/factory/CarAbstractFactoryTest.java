package indi.yh.samples.pattern.create.pattern1.factory;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.benz.BenzCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.bmw.BmwCar;
import indi.yh.samples.pattern.create.pattern1.factory.type.abstractFactory.BusinessDriverFactory;
import indi.yh.samples.pattern.create.pattern1.factory.type.abstractFactory.DriverAbstractFactory;
import indi.yh.samples.pattern.create.pattern1.factory.type.abstractFactory.SportDriverFactory;

public class CarAbstractFactoryTest {
	
	public static void main(String[] args) throws Exception {
		DriverAbstractFactory driverAbstractFactory = new BusinessDriverFactory();
		AudiCar audiCar = driverAbstractFactory.createAudiCar("");
		audiCar.drive();
		
		BenzCar benzCar = driverAbstractFactory.createBenzCar("");
		benzCar.drive();

		BmwCar bmwCar = driverAbstractFactory.createBmwCar("");
		bmwCar.drive();
		
		
		
		driverAbstractFactory = new SportDriverFactory();
		audiCar = driverAbstractFactory.createAudiCar("");
		audiCar.drive();
		
		benzCar = driverAbstractFactory.createBenzCar("");
		benzCar.drive();

		bmwCar = driverAbstractFactory.createBmwCar("");
		bmwCar.drive();
	}
}
