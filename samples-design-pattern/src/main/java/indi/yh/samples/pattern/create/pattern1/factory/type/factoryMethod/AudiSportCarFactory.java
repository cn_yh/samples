package indi.yh.samples.pattern.create.pattern1.factory.type.factoryMethod;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiSportCar;

public class AudiSportCarFactory implements AudiCarFactoryMethod {

	public AudiCar getAudiCar() {
		return new AudiSportCar();
	}

}
