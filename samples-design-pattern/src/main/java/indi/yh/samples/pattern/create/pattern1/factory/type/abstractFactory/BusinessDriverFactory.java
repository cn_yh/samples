package indi.yh.samples.pattern.create.pattern1.factory.type.abstractFactory;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiBusinessCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.benz.BenzBusinessCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.benz.BenzCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.bmw.BmwBusinessCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.bmw.BmwCar;

public class BusinessDriverFactory extends DriverAbstractFactory {

	@Override
	public BenzCar createBenzCar(String car) throws Exception {
		return new BenzBusinessCar();
	}

	@Override
	public BmwCar createBmwCar(String car) throws Exception {
		return new BmwBusinessCar();
	}

	@Override
	public AudiCar createAudiCar(String car) throws Exception {
		return new AudiBusinessCar();
	}

}
