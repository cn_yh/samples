package indi.yh.samples.pattern.create.pattern2.singleton.lazySync;

/**
 * 
 * 描述: 懒汉式，线程安全<br/>
 * 
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月25日 下午2:01:02
 * @version V1.0
 */
public class LazySyncSingleton {
	private static LazySyncSingleton instance;

	private LazySyncSingleton() {
	}

	public static synchronized LazySyncSingleton getInstance() {
		if (instance == null) {
			instance = new LazySyncSingleton();
		}
		return instance;
	}

}
