package indi.yh.samples.pattern.create.pattern2.singleton.lazy;

/**
 * 
 * 描述: 懒汉式，线程不安全<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月25日 下午1:55:34
 * @version V1.0
 */
public class LazySingleton {

	private static LazySingleton instance;

	private LazySingleton() {
	}

	public static LazySingleton getInstance() {
		if (instance == null) {
			instance = new LazySingleton();
		}
		return instance;
	}

}
