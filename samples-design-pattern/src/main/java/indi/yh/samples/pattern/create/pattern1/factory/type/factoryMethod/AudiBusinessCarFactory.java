package indi.yh.samples.pattern.create.pattern1.factory.type.factoryMethod;

import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiBusinessCar;
import indi.yh.samples.pattern.create.pattern1.factory.car.audi.AudiCar;

public class AudiBusinessCarFactory implements AudiCarFactoryMethod {

	public AudiCar getAudiCar() {
		return new AudiBusinessCar();
	}

}
