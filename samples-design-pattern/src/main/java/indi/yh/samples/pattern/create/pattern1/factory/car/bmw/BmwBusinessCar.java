package indi.yh.samples.pattern.create.pattern1.factory.car.bmw;

public class BmwBusinessCar extends BmwCar {

	@Override
	public void drive() {
        System.out.println("----BmwBusinessCar-----------------------");  		
	}

}
