package indi.yh.samples.pattern.create.pattern3.builder;

import indi.yh.samples.pattern.create.pattern3.builder.packing.Packing;

public interface Item {
	
	String name();

	Packing packing();

	float price();

}
