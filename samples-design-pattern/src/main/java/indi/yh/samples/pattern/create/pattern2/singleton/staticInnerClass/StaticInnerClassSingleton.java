package indi.yh.samples.pattern.create.pattern2.singleton.staticInnerClass;

/**
 * 
 * 描述: 登记式/静态内部类<br/>
 *
 * @author 于鸿[yuhongemail@163.com]
 * @date 2017年8月25日 下午2:16:16
 * @version V1.0
 */
public class StaticInnerClassSingleton {
	
	private static class SingletonHolder {
		private static final StaticInnerClassSingleton INSTANCE = new StaticInnerClassSingleton();
	}

	private StaticInnerClassSingleton() {
	}

	public static final StaticInnerClassSingleton getInstance() {
		return SingletonHolder.INSTANCE;
	}

}