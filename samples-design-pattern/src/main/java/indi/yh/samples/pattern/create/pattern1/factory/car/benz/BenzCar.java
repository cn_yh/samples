package indi.yh.samples.pattern.create.pattern1.factory.car.benz;

public abstract class BenzCar {

	public abstract void drive();

}
