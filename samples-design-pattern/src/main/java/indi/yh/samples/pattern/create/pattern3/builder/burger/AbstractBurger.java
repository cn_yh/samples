package indi.yh.samples.pattern.create.pattern3.builder.burger;

import indi.yh.samples.pattern.create.pattern3.builder.Item;
import indi.yh.samples.pattern.create.pattern3.builder.packing.Packing;
import indi.yh.samples.pattern.create.pattern3.builder.packing.Wrapper;

public abstract class AbstractBurger implements Item{

	@Override
	public Packing packing() {
		return new Wrapper();
	}

	@Override
	public abstract float price();

}
