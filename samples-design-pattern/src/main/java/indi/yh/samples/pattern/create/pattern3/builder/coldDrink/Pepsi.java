package indi.yh.samples.pattern.create.pattern3.builder.coldDrink;

public class Pepsi extends AbstractColdDrink{

	@Override
	public String name() {
		return "Pepsi";
	}

	@Override
	public float price() {
		return 35.0f;
	}

}
